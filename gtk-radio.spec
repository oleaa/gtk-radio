Name:           gtk-radio
Version:        384.0
Release:        1%{?dist}
Summary:        GTK Radio
License:        GPLv3+
URL:            http://www.gnomeradio.org/~ole/radio/
Source:         %{url}/%{name}-%{version}.tar.xz

BuildRequires:  gtk4-devel
BuildRequires:  pango
BuildRequires:  libchamplain-devel
BuildRequires:  libxml2-devel
BuildRequires:  intltool
BuildRequires:  itstool
BuildRequires:  libappstream-glib
BuildRequires:  desktop-file-utils
BuildRequires:  geocode-glib-devel
BuildRequires:  gstreamer1-devel
BuildRequires:  gstreamer1-plugins-bad-free-devel
BuildRequires:  gstreamer1-plugins-base-devel
Requires:       gstreamer1 >= 1.8.3
Requires:       gstreamer1-plugins-ugly-free >= 1.8.3
Requires:       geocode-glib >= 3.20.1
Obsoletes:      gnome-radio <= 64.0.45
Provides:       gnome-radio = 64.0.45
Obsoletes:	gtk-internet-radio-locator <= 128.0
Provides:	gtk-internet-radio-locator >= 128.0

%description
GTK Radio is a Free Software program that allows you to
easily locate and listen to Free Internet Radio stations by
broadcasters on the Internet with the help of a map.

GTK Radio is developed on for the brand
new GTK platform and it requires gstreamer 1.0 for playback.

Enjoy Free Internet Radio.

%prep
%setup -q

%build
%configure --with-recording --disable-silent-rules --disable-schemas

%install
%make_install
%find_lang %{name} --with-man

%check
appstream-util validate-relax --nonet %{buildroot}/%{_datadir}/appdata/%{name}.appdata.xml
desktop-file-validate %{buildroot}/%{_datadir}/applications/%{name}.desktop
%post
%files -f %{name}.lang
%doc AUTHORS DEBIAN NEWS README TODO ChangeLog
%license COPYING
%{_bindir}/%{name}
%{_bindir}/org.gnome.Radio
%{_datadir}/gtk-radio/internet-radio-locator-384.0.dtd
%{_datadir}/gtk-radio/internet-radio-locator.xml
%{_datadir}/icons/hicolor/scalable/apps/gtk-radio.svg
%{_mandir}/man1/gtk-radio.1.gz
%{_datadir}/org.gnome.Radio/org.gnome.Radio.dtd
%{_datadir}/org.gnome.Radio/org.gnome.Radio.xml
%{_datadir}/%{name}/
%{_datadir}/appdata/%{name}.appdata.xml
%{_datadir}/applications/%{name}.desktop
%{_mandir}/man1/%{name}.1*
%{_datadir}/icons/hicolor/1024x1024/apps/gtk-radio.png
%{_datadir}/icons/hicolor/16x16/apps/gtk-radio.png
%{_datadir}/icons/hicolor/22x22/apps/gtk-radio.png
%{_datadir}/icons/hicolor/24x24/apps/gtk-radio.png
%{_datadir}/icons/hicolor/256x256/apps/gtk-radio.png
%{_datadir}/icons/hicolor/32x32/apps/gtk-radio.png
%{_datadir}/icons/hicolor/48x48/apps/gtk-radio.png
%{_datadir}/icons/hicolor/512x512/apps/gtk-radio.png
%{_datadir}/icons/hicolor/scalable/apps/gtk-radio.svg

%changelog
* Tue Mar 26 2024 Ole Aamot <ole@aamot.org> - 384.0-1
- gtk-radio 384.0 build on Fedora Linux 40

* Mon Mar 11 2024 Ole Aamot <ole@aamot.org> - 264.3-1
- gtk-radio 264.3 build on GNOME 46

* Wed Feb 28 2024 Ole Aamot <ole@aamot.org> - 264.2-1
- gtk-radio 264.2 build on Fedora Linux 40

* Mon Jan 22 2024 Ole Aamot <ole@aamot.org> - 264.1-1
- gtk-radio 264.1 build on Fedora Linux 40

* Thu Jan 18 2024 Ole Aamot <ole@aamot.org> - 264.0-1
- gtk-radio 264.0 build on Fedora Linux 40
