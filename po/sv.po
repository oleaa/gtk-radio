# Swedish translation for gtk-internet-radio-locator.
# Copyright © 2017, 2018 gtk-internet-radio-locator's COPYRIGHT HOLDER
# This file is distributed under the same license as the gtk-internet-radio-locator package.
# Anders Jonsson <anders.jonsson@norsjovallen.se>, 2017, 2018.
# Josef Andersson <josef.andersson@fripost.org>, 2018.
#
msgid ""
msgstr ""
"Project-Id-Version: gtk-internet-radio-locator master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gtk-internet-radio-"
"locator/issues\n"
"POT-Creation-Date: 2018-05-30 08:14+0000\n"
"PO-Revision-Date: 2018-06-27 14:24+0200\n"
"Last-Translator: Anders Jonsson <anders.jonsson@norsjovallen.se>\n"
"Language-Team: Swedish <tp-sv@listor.tp-sv.se>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 2.0.8\n"

#: ../data/gtk-internet-radio-locator.appdata.xml.in.h:1
#: ../data/gtk-internet-radio-locator.desktop.in.in.h:1
msgid "Internet Radio Locator"
msgstr "Internetradioletare"

#: ../data/gtk-internet-radio-locator.appdata.xml.in.h:2
msgid "Listen to Internet radio"
msgstr "Lyssna på internetradio"

#: ../data/gtk-internet-radio-locator.appdata.xml.in.h:3
msgid ""
"Internet Radio Locator allows users to easily find and listen to live radio "
"programs on radio broadcasters on the Internet."
msgstr ""
"Internetradioletare låter användare lätt hitta och lyssna på radioprogram "
"direkt från sändningar på internet."

#: ../data/gtk-internet-radio-locator.appdata.xml.in.h:4
msgid "Internet Radio Locator is developed for GTK+ 4.0."
msgstr "Internetradioletare utvecklas för GTK+ 4.0."

#: ../data/gtk-internet-radio-locator.appdata.xml.in.h:5
msgid ""
"Enjoy listening to and recording from supported Internet Radio stations."
msgstr "Njut av att lyssna och spela in från internetradiostationer som stöds."

#: ../data/gtk-internet-radio-locator.desktop.in.in.h:2
msgid "Locate Internet Radio Stations"
msgstr "Leta rätt på internetradiostationer"

#. Extra keywords that can be used to search for gtk-internet-radio-locator in GNOME Shell and Unity
#: ../data/gtk-internet-radio-locator.desktop.in.in.h:4
msgid "Live;Radio;Program;Station;Locator;"
msgstr "Live;Direktsänt;Radio;Program;Station;Locator;Letare;"

#: ../src/gtk-internet-radio-locator-gui.c:99
#: ../src/gtk-internet-radio-locator-gui.c:113
#: ../src/gtk-internet-radio-locator-gui.c:325
#: ../src/gtk-internet-radio-locator.c:866
#, c-format
msgid "Failed to open %s\n"
msgstr "Misslyckades med att öppna %s\n"

#. The Stations dialog
#: ../src/gtk-internet-radio-locator-gui.c:264
msgid "New Internet Radio Station"
msgstr "Ny internetradiostation"

#: ../src/gtk-internet-radio-locator-gui.c:267
msgid "_Save"
msgstr "_Spara"

#: ../src/gtk-internet-radio-locator-gui.c:289
msgid "Station name"
msgstr "Stationsnamn"

#: ../src/gtk-internet-radio-locator-gui.c:290
msgid "Bandwidth"
msgstr "Bandbredd"

#: ../src/gtk-internet-radio-locator-gui.c:292
msgid "City name"
msgstr "Stadens namn"

#: ../src/gtk-internet-radio-locator-gui.c:296
msgid "http://uri-to-stream/"
msgstr "http://uri-till-ström/"

#: ../src/gtk-internet-radio-locator-gui.c:297
msgid "Description"
msgstr "Beskrivning"

#: ../src/gtk-internet-radio-locator-gui.c:298
msgid "http://uri-to-website/"
msgstr "http://uri-till-webbplats/"

#: ../src/gtk-internet-radio-locator-gui.c:311
#, c-format
msgid "Failed to open %s.  Please install it.\n"
msgstr "Misslyckades med att öppna %s.  Vänligen installera det.\n"

#: ../src/gtk-internet-radio-locator-gui.c:459
#: ../src/gtk-internet-radio-locator.c:380
msgid "http://fm939.wnyc.org/wnycfm"
msgstr "http://fm939.wnyc.org/wnycfm"

#: ../src/gtk-internet-radio-locator-gui.c:462
msgid "WNYC"
msgstr "WNYC"

#: ../src/gtk-internet-radio-locator-gui.c:465
msgid "New York City, NY"
msgstr "New York City, NY"

#: ../src/gtk-internet-radio-locator-gui.c:468
msgid "ONLINE"
msgstr "ANSLUTEN"

#: ../src/gtk-internet-radio-locator-gui.c:471
msgid ""
"WNYC 93.9 FM and AM 820 are New York's flagship public radio stations, "
"broadcasting the finest programs from NPR, American Public Media, Public "
"Radio International and the BBC World Service, as well as a wide range of "
"award-winning local programming."
msgstr ""
"WNYC 93.9 FM och AM 820 är New Yorks främsta öppna radiostationer, och "
"sänder de bästa programmen från NPR, American Public Media, Public Radio "
"International och BBC World Service, såväl som en bred omfattning av prisade "
"lokala program."

#. printf("Archiving program at %s\n", archive);
#: ../src/gtk-internet-radio-locator-station.c:129
#, c-format
msgid "Recording from %s in %s to %s"
msgstr "Spelar in från %s i %s till %s"

#: ../src/gtk-internet-radio-locator-station.c:147
#, c-format
msgid ""
"An error happened trying to play %s\n"
"Either the file doesn't exist, or you don't have a player for it."
msgstr ""
"Ett fel uppstod vid uppspelning av %s\n"
"Antingen existerar inte filen, eller så har du ingen spelare för den."

#: ../src/gtk-internet-radio-locator-station.c:153
#, c-format
msgid ""
"An error happened trying to record %s\n"
"Either the file doesn't exist, or you don't have a recorder for it."
msgstr ""
"Ett fel uppstod vid inspelning av %s\n"
"Antingen existerar inte filen, eller så har du ingen inspelare för den."

#: ../src/gtk-internet-radio-locator-station.c:184
#: ../src/gtk-internet-radio-locator-station.c:204
#: ../src/gtk-internet-radio-locator-station.c:321
#: ../src/gtk-internet-radio-locator-station.c:344
#, c-format
msgid "Failed to run %s (%i)\n"
msgstr "Kunde inte köra %s (%i)\n"

#: ../src/gtk-internet-radio-locator-station.c:231
#: ../src/gtk-internet-radio-locator-station.c:368
#, c-format
msgid ""
"Failed to open URL: '%s'\n"
"Status code: %i\n"
"Details: %s"
msgstr ""
"Kunde inte öppna URL: ”%s”\n"
"Statuskod: %i\n"
"Detaljer: %s"

#: ../src/gtk-internet-radio-locator-station.c:249
#: ../src/gtk-internet-radio-locator-station.c:263
#, c-format
msgid ""
"Failed to open URL: '%s'\n"
"Details: %s"
msgstr ""
"Kunde inte öppna URL: ”%s”\n"
"Detaljer: %s"

#: ../src/gtk-internet-radio-locator.c:364
#, c-format
msgid "New Internet Radio Station\n"
msgstr "Ny internetradiostation\n"

#: ../src/gtk-internet-radio-locator.c:370
#, c-format
msgid "Search Internet Radio Station\n"
msgstr "Sök efter internetradiostation\n"

#: ../src/gtk-internet-radio-locator.c:405
#, c-format
msgid "Previous Internet Radio Station\n"
msgstr "Föregående internetradiostation\n"

#: ../src/gtk-internet-radio-locator.c:539
msgid "Search"
msgstr "Sök"

#: ../src/gtk-internet-radio-locator.c:543
msgid "Search Internet Radio Station"
msgstr "Sök efter internetradiostation"

#: ../src/gtk-internet-radio-locator.c:548
msgid "Prev"
msgstr "Föregående"

#: ../src/gtk-internet-radio-locator.c:552
msgid "Prev Internet Radio Station"
msgstr "Föregående internetradiostation"

#: ../src/gtk-internet-radio-locator.c:555
#: ../src/gtk-internet-radio-locator.c:559
msgid "Stations"
msgstr "Stationer"

#: ../src/gtk-internet-radio-locator.c:562
msgid "Next"
msgstr "Nästa"

#: ../src/gtk-internet-radio-locator.c:566
msgid "Next Internet Radio Station"
msgstr "Nästa internetradiostation"

#: ../src/gtk-internet-radio-locator.c:569
#: ../src/gtk-internet-radio-locator.c:573
msgid "About Station"
msgstr "Om station"

#: ../src/gtk-internet-radio-locator.c:576
#: ../src/gtk-internet-radio-locator.c:580
msgid "About Program"
msgstr "Om programmet"

#. give it the title
#: ../src/gtk-internet-radio-locator.c:592
#: ../src/gtk-internet-radio-locator.c:762
msgid "Internet Radio Locator for GTK+ 4.0"
msgstr "Internetradioletare för GTK+ 4.0"

#: ../src/gtk-internet-radio-locator.c:637
#, c-format
msgid ""
"Nothing\n"
"\n"
msgstr ""
"Ingenting\n"
"\n"

#: ../src/gtk-internet-radio-locator.c:925
msgid "Stop"
msgstr "Stoppa"

#: ../src/gtk-internet-radio-locator.c:931
msgid "Zoom In"
msgstr "Zooma in"

#: ../src/gtk-internet-radio-locator.c:945
msgid "Zoom Out"
msgstr "Zooma ut"

#: ../src/gtk-internet-radio-locator.c:950
msgid "Markers"
msgstr "Markörer"

#: ../src/gtk-internet-radio-locator.c:972
msgid "Exit"
msgstr "Avsluta"

#~ msgid "Locate and listen to Free Internet Radio stations"
#~ msgstr "Leta rätt och lyssna på fria internetradiostationer"

#~ msgid ""
#~ "GNOME Internet Radio Locator for GNOME 3 is developed for the GNOME 3 "
#~ "desktop and requires gstreamer 1.0 to be installed for listening to Free "
#~ "Internet Radio stations."
#~ msgstr ""
#~ "GNOME:s internetradioletare för GNOME 3 utvecklas för GNOME 3-skrivbordet "
#~ "och kräver att gstreamer 1.0 finns installerat för uppspelning av fria "
#~ "internetradiostationer."

#~ msgid ""
#~ "Enjoy listening to Free Internet Radio stations in GNOME Internet Radio "
#~ "Locator for GNOME 3."
#~ msgstr ""
#~ "Njut av att lyssna på fria internetradiostationer i GNOME:s "
#~ "internetradioletare för GNOME 3."

#~ msgid "Default station"
#~ msgstr "Standardstation"

#~ msgid "Default Internet Radio station."
#~ msgstr "Standardstation för internetradio."
